# Webarchitects GitLab Ansible Role

This role is designed to install GitLab CE on servers with `gitlab_ce` set to `true` and GitLab Runner on servers with `gitlab_runner` set to `true`.

## GitLab CE

TODO.

## GitLab Runner

See the [Install GitLab Runner using the official GitLab repositories](https://docs.gitlab.com/runner/install/linux-repository.html) and [Registering Runners](https://docs.gitlab.com/runner/register/index.html) documents on [GitLab Docs](https://docs.gitlab.com/).

First run the [Docker](https://git.coop/webarch/docker) playbook and then this role and then get the registration token from [git.coop/admin/runners](https://git.coop/admin/runners) and then run:

```bash
sudo -i
gitlab-runner register
```

And enter the following:

```txt
Please enter the gitlab-ci coordinator URL (e.g. https://gitlab.com/):
https://git.coop/
Please enter the gitlab-ci token for this runner:
XXXX
Please enter the gitlab-ci description for this runner:
[runner]: runner.git.coop
Please enter the gitlab-ci tags for this runner (comma separated):

Whether to lock the Runner to current project [true/false]:
[true]: false
Please enter the executor: shell, ssh, kubernetes, docker, parallels, virtualbox, docker+machine, docker-ssh+machine, docker-ssh:
docker
Please enter the default Docker image (e.g. ruby:2.1):
debian:stable
```

For the privileged runner we to use the [docker-in-docker executor](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker-executor) so `/etc/gitlab-runner/config.toml` needs these lines in the `[runners.docker]` section (see [this thread](https://gitlab.com/gitlab-com/support-forum/issues/795):

```ini
    tls_verify = true
    privileged = true
    volumes = ["/cache","/certs/client"]
```

The default non-privileged runner has `privileged = false`.

And also increase the resources available and configure the DNS servers:

```ini
    memory = "8192m"
    memory_swap = "2048m"
    memory_reservation = "8192m"
    shm_size = 536870912
    cpus = "4"
    dns = ["81.95.52.24", "81.95.52.30"]
```

Increase the `output_limit` for the [job log length](https://gitlab.com/gitlab-com/support-forum/issues/2790) in the `[[runners]]` section:

```ini
  output_limit = 20480
```

And add the [overlay2 filesystem driver](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#using-the-overlayfs-driver):

```ini
  environment = ["DOCKER_DRIVER=overlay2"]
```

Also run the [ansible](https://git.coop/webarch/ansible) and [sudoers](https://git.coop/webarch/sudoers) Playbooks.

## GitLab Registry

See the [GitLab Container Registry administration](https://docs.gitlab.com/ee/administration/packages/container_registry.html) page for help.

In `/etc/gitlab/gitlab.rb` we already have:

```ruby
nginx['custom_gitlab_server_config'] = 'location ^~ /.well-known { alias /var/www/.well-known; } '
```

And `acme.sh` is provisioning the certs (this was setup before GitLab could get Let's Encrypt certs itself).

Edit `/etc/gitlab/gitlab.rb` to add:

```ruby
registry_external_url 'https://registry.git.coop'
```

And reconfigure:

```bash
gitlab-ctl reconfigure
```

Check that we can access files on the domain:

```bash
lynx -dump http://registry.git.coop/.well-known/acme-challenge/foo
bar
```

Generate a new RSA cert (it doesn't work with a ECC one it seems) and install it:

```bash
/root/.acme.sh/acme.sh --issue -d registry.git.coop -w /var/www
/root/.acme.sh/acme.sh --installcert -d registry.git.coop --keypath /etc/gitlab/ssl/registry.git.coop.key --fullchainpath /etc/gitlab/ssl/registry.git.coop.crt --reloadcmd "/opt/gitlab/init/nginx restart"
```

Edit `/etc/gitlab/gitlab.rb` to add:

```ruby
registry_external_url 'https://registry.git.coop'
registry_nginx['ssl_certificate'] = "/etc/gitlab/ssl/registry.git.coop.crt"
registry_nginx['ssl_certificate_key'] = "/etc/gitlab/ssl/registry.git.coop.key"
```

And reconfigure and restart:

```bash
gitlab-ctl reconfigure && gitlab-ctl restart
```
